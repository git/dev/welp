# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Full sources including CFS and HRT patches"
HOMEPAGE="http://dev.gentoo.org/~angelos/"

KEYWORDS="~amd64"
IUSE=""

K_WANT_GENPATCHES="base"
K_GENPATCHES_VER="9"
UNIPATCH_STRICTORDER="1"
ETYPE="sources"
inherit kernel-2

detect_version

UNIPATCH_LIST="${DISTDIR}/${PF}.patch.bz2"

SRC_URI="${KERNEL_URI} ${GENPATCHES_URI}
	http://dev.gentoo.org/~angelos/patches/${PF}.patch.bz2"
