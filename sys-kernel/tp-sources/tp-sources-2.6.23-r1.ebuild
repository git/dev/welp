# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-kernel/gentoo-sources/gentoo-sources-2.6.23-r1.ebuild,v 1.1 2007/11/02 16:29:10 mpagano Exp $

DESCRIPTION="Full sources including Gentoo's patchsets and patches for \
    ThinkPad laptops"
HOMEPAGE="http://dev.gentoo.org/~welp/foo/"

KEYWORDS="~x86"
IUSE=""

K_WANT_GENPATCHES="base extras"
K_GENPATCHES_VER="2"
UNIPATCH_STRICTORDER="1"
ETYPE="sources"
inherit kernel-2

detect_version

UNIPATCH_LIST="${DISTDIR}/patch-${OKV}-tp.patch.bz2"

SRC_URI="${KERNEL_URI} ${GENPATCHES_URI}
    http://dev.gentooexperimental.org/~welp/gentoo/tp-sources/${OKV}/patch-${OKV}-tp.patch.bz2"
