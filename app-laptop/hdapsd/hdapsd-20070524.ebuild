# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-laptop/hdapsd/hdapsd-20060409-r1.ebuild,v 1.3 2007/08/29 18:55:52 genstef Exp $

inherit eutils linux-info

PROTECT_VER="2"

DESCRIPTION="IBM ThinkPad Harddrive Active Protection disk head parking daemon"
HOMEPAGE="http://hdaps.sourceforge.net/"
SRC_URI="http://dev.gentooexperimental.org/~welp/gentoo/hdapsd/${P}.c.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

IUSE=""
RDEPEND="app-laptop/tp_smapi"

S="${WORKDIR}"

src_compile() {
	cd "${WORKDIR}"
	gcc ${CFLAGS} "${P}".c -o hdapsd || die "failed to compile"
}

src_install() {
	dosbin "${WORKDIR}"/hdapsd
	newconfd "${FILESDIR}"/hdapsd.conf hdapsd
	newinitd "${FILESDIR}"/hdapsd.init hdapsd
}

# Yes, this sucks as the source location may change, kernel sources may not be
# installed, but we try our best anyway
kernel_patched() {
	get_version

	if grep -qs "blk_protect_register" "${KERNEL_DIR}"/block/ll_rw_blk.c ; then
		return 0
	fi

	return 1
}

pkg_postinst(){
	[[ -n $(ls "${ROOT}"/sys/block/*/queue/protect 2>/dev/null) ]] && return 0

	if ! kernel_patched ; then
		ewarn "Your kernel has NOT been patched for blk_freeze"
		elog "You should install tp-sources from welp's overlay"
	fi
}
